import { useState, useEffect } from 'react'
interface FetchResult<T> {
  data: T | null
  loading: boolean
  error: Error | null
}
export const useFetch = (url: string): FetchResult<any> => {
  const [data, setData] = useState<any | null>(null)
  const [loading, setLoading] = useState<boolean>(true)
  const [error, setError] = useState<Error | null>(null)
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(url)
        if (!response.ok) {
          throw new Error('Network response was not ok')
        }
        const json = await response.json()
        setData(json)
        setLoading(false)
      } catch (error: any) {
        setError(error)
        setLoading(false)
      }
    }
    fetchData()
  }, [url])

  return { data, loading, error }
}
