import React from 'react'
const Image = ({ src }: { src: string }) => {
  return (
    <img
      src={src}
      alt="Avatar"
      data-testid={`user-avatar`}
      className="rounded-full  w-[100px] h-[100px] object-cover"
    />
  )
}
export default Image
