import React from 'react'
import { render } from '@testing-library/react'
import Image from './index'

describe('Image component', () => {
  it('renders the image with the correct src', () => {
    const src = 'https://example.com/avatar.png'
    const { getByAltText } = render(<Image src={src} />)
    const imageElement = getByAltText('Avatar')
    expect(imageElement).toBeInTheDocument()
    expect(imageElement).toHaveAttribute('src', src)
  })

  it('applies the correct className', () => {
    const src = 'https://example.com/avatar.png'
    const { container } = render(<Image src={src} />)
    const imageElement = container.querySelector('img')
    expect(imageElement).toHaveClass('rounded-full w-[100px] h-[100px] object-cover')
  })
})
