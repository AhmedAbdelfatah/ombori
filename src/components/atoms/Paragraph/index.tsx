import React from 'react'
export default function Paragraph({ text }: { text: string }) {
  return <p className="flex justify-center items-center py-8 text-xl">{text}</p>
}
