import React from 'react'
import { render } from '@testing-library/react'
import Paragraph from './index'

describe('Paragraph component', () => {
  it('renders the text correctly', () => {
    const text = 'Sample text'
    const { getByText } = render(<Paragraph text={text} />)
    const paragraphElement = getByText(text)
    expect(paragraphElement).toBeInTheDocument()
  })

  it('applies the correct className', () => {
    const text = 'Sample text'
    const { container } = render(<Paragraph text={text} />)
    const paragraphElement = container.querySelector('p')
    expect(paragraphElement).toHaveClass('flex justify-center items-center py-8 text-xl')
  })
})
