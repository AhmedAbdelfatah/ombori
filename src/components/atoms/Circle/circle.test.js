import React from 'react'
import { render } from '@testing-library/react'
import Circle from './index'

describe('Circle component', () => {
  it('renders the circle with correct classes', () => {
    const color = 'blue'
    const width = '50px'
    const height = '50px'
    const { container } = render(<Circle color={color} width={width} height={height} />)
    const circleElement = container.querySelector('.circle')
    expect(circleElement).toBeInTheDocument()
    expect(circleElement).toHaveClass('blue')
    expect(circleElement).toHaveClass('50px')
    expect(circleElement).toHaveClass('50px')
    expect(circleElement).toHaveClass('rounded-full')
  })

  it('renders the centered circle with correct classes when animate prop is false', () => {
    const color = 'red'
    const width = '60px'
    const height = '60px'
    const { container } = render(
      <Circle color={color} width={width} height={height} animate={false} />
    )
    const circleElement = container.querySelector('.centered-circle')
    expect(circleElement).toBeInTheDocument()
    expect(circleElement).toHaveClass('red')
    expect(circleElement).toHaveClass('60px')
    expect(circleElement).toHaveClass('60px')
    expect(circleElement).toHaveClass('rounded-full')
  })
})
