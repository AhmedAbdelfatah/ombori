import React from 'react'
import clsx from 'clsx'
import './circle.css'

type CircleProps = {
  color: string
  width: string
  height: string
  animate?: boolean
}
const Circle = ({ color, width, height, animate = true }: CircleProps) => {
  const circleClasses = clsx(
    animate ? 'circle' : 'centered-circle',
    `${color}`,
    `${width}`,
    `${height}`,
    `rounded-full`
  )
  return <div className={circleClasses}></div>
}
export default Circle
