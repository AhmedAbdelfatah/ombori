import React, { useState, useEffect } from 'react'
import axios from 'axios'
import InfiniteScroll from 'react-infinite-scroll-component'
import { nanoid } from 'nanoid'
import { EndPoints } from '../../../constants/api'
import Card from '../../molecules/Card'
import Loader from '../../molecules/Loader'
import Header from '../../molecules/Header'

export default function List() {
  const [listItems, setListItems] = useState([])
  const [page, setPage] = useState(1)
  const [hasMore, setHasMore] = useState(true)

  useEffect(() => {
    setTimeout(() => {
      fetchData()
    }, 3000)
  }, [])

  const fetchData = async () => {
    const response = await axios.get(`${EndPoints.Users}?page=${page}`)
    if (response.data['data'].length > 0) {
      setListItems(listItems.concat(response.data['data']))
      setPage(page + 1)
    } else {
      setHasMore(false)
    }
  }

  return (
    <>
      <Header text="Users" />
      <InfiniteScroll
        dataLength={listItems.length}
        next={fetchData}
        hasMore={hasMore}
        loader={<Loader />}
        className="flex flex-col justify-center mx-auto p-4 w-full"
      >
        {listItems.map((item: any) => {
          // eslint-disable-next-line react/jsx-key
          return (
            <Card
              key={nanoid()}
              name={`${item.first_name} ${item.last_name}`}
              profileImage={item.avatar}
            />
          )
        })}
      </InfiniteScroll>
    </>
  )
}
