import React, { useState, useEffect } from 'react'
import Circle from '../../atoms/Circle'
export default function Loader() {
  const [showLoader, setShowLoader] = useState(true)
  useEffect(() => {
    const timer = setTimeout(() => {
      setShowLoader(false)
    }, 3000)

    return () => {
      clearTimeout(timer)
    }
  }, [])
  return (
    <>
      {showLoader && (
        <>
          <div className="flex justify-center items-center h-[100px]">
            <Circle width="w-[20px]" height="h-[20px]" color="bg-[#7fb900]" animate={false} />
            <Circle width="w-[80px]" height="h-[80px]" color="bg-[#d0e5a3]" />
            <Circle width="w-[110px]" height="h-[110px]" color="bg-[#e5f1cc]" />
          </div>
        </>
      )}
    </>
  )
}
