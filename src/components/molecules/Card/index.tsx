import React, { Suspense } from 'react'
import Image from '../../atoms/Image'
import avatar from './avatar.png'
type CardProps = {
  profileImage: string
  name: string
}
const LazyLoadImage = React.lazy(() => {
  return import('../../atoms/Image')
})
export default function Card({ profileImage, name }: CardProps) {
  return (
    <div
      className="flex md:justify-center items-center p-8 border-solid border-b-2 border-[#EEE]"
      data-testid="mocked-image"
    >
      <Suspense fallback={<Image src={avatar} />}>
        <LazyLoadImage src={profileImage} />
      </Suspense>
      <p className="flex-grow pl-4">{name}</p>
    </div>
  )
}
