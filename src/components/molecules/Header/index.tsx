import React from 'react'
import Paragraph from '../../atoms/Paragraph'

export default function Header({ text = 'Header' }: { text: string }) {
  return (
    <div className="bg-[#F1F1F1]">
      <Paragraph text={text} />
    </div>
  )
}
