import React from 'react'
import { render } from '@testing-library/react'
import Header from './index'

describe('Header component', () => {
  it('renders the header text correctly', () => {
    const headerText = 'Sample Header'
    const { getByText } = render(<Header text={headerText} />)

    const headerElement = getByText(headerText)

    expect(headerElement).toBeInTheDocument()
  })
})
