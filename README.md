## :ledger: Users list API

If you visit http://localhost:3000 you will find a list of users pagniated from the given source **`https://reqres.in/users`** which returns a list of users.

- :pushpin: [**Given Requirements**](https://github.com/ombori/fullstack-code-test-ts/tree/master)

## Important links

- :rocket: :rocket: [Deployment link](https://ombori.onrender.com/) :point_left: :point_left:

## Tech-Stack

- :office: Scaffolding ( Husky , prettier , linter)
- :dart: CI/CD (Gitlab pipline with the following stages)
  - :one: Install dependancies.
  - :two: Building the code.
  - :three: Testing
    - code style test
- :steam_locomotive: API (REST)
- :telephone: Git [trunk based development](https://gitlab.com/AhmedAbdelfatah/ombori/-/branches)
- :rocket: Deployment (Docker,CI/CD) [Ombori](https://ombori.onrender.com/)
- :white_check_mark: Testing using `jest` and `react testing library`
- :rocket: Documentation (Markdown README.md)

## :eyes: Getting Started

Clone the project , install the dependancies and run it locally as development or you can run it using docker by running the following command `npm run docker`

### :bangbang: Prerequisites

Make sure that your environment setup has the follwoing installed on your machine:

- npm & node
- Typescript
- Docker If you want run the image locally
- Git

### :gear: Running locally from dev environment

- Clone the repo by running the following command
  ```
  git clone https://gitlab.com/AhmedAbdelfatah/ombori.git
  cd ombori
  ```
- Install the dependancies by running the following command

  ```
  npm i
  ```

- Running the project
  ```
  npm run start
  ```

:triangular_flag_on_post: Visit this url https://localhost:3000

### :gear: Running the project by using docker

You can run the project via docker just make sure that docker installed on your machine then run the following command:

```
npm run docker
```

:triangular_flag_on_post: Then visit this url https://localhost:3000

### Unit testing
:heavy_check_mark: To run the tests use the following command:
```
npm run test
```
